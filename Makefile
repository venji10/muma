#    Copyright (C) 2021-2022 venji10 <venji10@riseup.net>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

CC=gcc
LIBS=libtoml libcurl taglib_c
CFLAGS=-Wall -Wextra `pkg-config --cflags $(LIBS)`
LDFLAGS=`pkg-config --libs $(LIBS)`

# to compile for debug: make DEBUG=1
ifdef DEBUG
	CFLAGS += -fno-omit-frame-pointer -fsanitize=address -g
	LDFLAGS += -fsanitize=address -g
endif

EXECUTABLE=muma
MANPAGE=muma.1
SRCDIR=src

MUMA_VERSION="v0.1.1"

$(EXECUTABLE): $(SRCDIR)/main.c actions.o audiomod.o creator.o downloader.o parser.o song.o ytdl.o
	$(CC) -DMUMA_VERSION='$(MUMA_VERSION)' $(CFLAGS) $^ $(LDFLAGS) -o $(EXECUTABLE)

actions.o: $(SRCDIR)/actions.c $(SRCDIR)/actions.h $(SRCDIR)/colors.h
	$(CC) -c $(CFLAGS) $(SRCDIR)/actions.c $(LDFLAGS)

audiomod.o: $(SRCDIR)/audiomod.c $(SRCDIR)/audiomod.h $(SRCDIR)/colors.h
	$(CC) -c $(CFLAGS) $(SRCDIR)/audiomod.c $(LDFLAGS)

creator.o: $(SRCDIR)/creator.c $(SRCDIR)/creator.h $(SRCDIR)/colors.h
	$(CC) -c $(CFLAGS) $(SRCDIR)/creator.c $(LDFLAGS)

downloader.o: $(SRCDIR)/downloader.c $(SRCDIR)/downloader.h $(SRCDIR)/colors.h
	$(CC) -c $(CFLAGS) $(SRCDIR)/downloader.c $(LDFLAGS)

parser.o: $(SRCDIR)/parser.c $(SRCDIR)/parser.h $(SRCDIR)/colors.h
	$(CC) -c $(CFLAGS) $(SRCDIR)/parser.c $(LDFLAGS)

song.o: $(SRCDIR)/song.c $(SRCDIR)/song.h $(SRCDIR)/colors.h
	$(CC) -c $(CFLAGS) $(SRCDIR)/song.c $(LDFLAGS)

ytdl.o: $(SRCDIR)/ytdl.c $(SRCDIR)/ytdl.h $(SRCDIR)/colors.h
	$(CC) -c $(CFLAGS) $(SRCDIR)/ytdl.c $(LDFLAGS)

install: $(EXECUTABLE)
	install -Dm755 $(EXECUTABLE) /usr/bin/
	install -Dm644 $(MANPAGE) /usr/share/man/man1/
	gzip /usr/share/man/man1/$(MANPAGE)

clean:
	rm -f $(EXECUTABLE) $(wildcard *.o)

.PHONY: install clean 

