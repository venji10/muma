## Main idea
+ Write a tool to help people relying less on proprietary music streaming services


+ Program for managing local music and downloading it from various sources (proprietary services like YouTube who have most popular music, also free services like libre.fm, kahvi)
+ Use text files (TOML) containing information about a song (title, author, category, download link, (alternative download link?), (timestamps for skipping unneeded scenes?), ...)
+ Those can easily be shared by simply copying them
+ And can be managed using git

## Advantages
+ Easily share playlists using git repos
+ Quicker sync between devices; don't have to push/pull all songs, only descriptions -> can download songs later without connection between devices
+ Organize music files and make them look good (metadata, filename, etc..)
+ Don't have to rely on playlists from Spotify, YouTube

## Planned features
# Main features
+ Provide functionality for downloading single songs from TOML files or entire playlists containing multiple songs
+ Provide functionality for creating song files using the program
+ support for various music hosting sites
+ search functionality for music offline (and online?)
+ support for splitting up big albums with multiple songs in them
+ support for getting playlists from YouTube into a description file

+ support for simple downloading with prompts for information on the piece of music

# Playlist management options
+ git integration
+ package multiple song files into one playlist file
+ also provide option for extracting a playlist file into multiple song files
+ option for adding (/removing?) single songs to (/from?) playlist files
+ option for extracting single song from playlist file
