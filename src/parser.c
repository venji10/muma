/*
 *   Copyright (C) 2022 venji10 <venji10@riseup.net>
 *
 *   parser.c: responsible for parsing TOML files. Also manages an array
 *   of song structs for managing all songs from parsed files.
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/* TODO:
 * split songs support
 * refine table_to_song function: use #define
 * for information keys
 * extend check_required function
 */
#include <toml.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include "parser.h"
#include "downloader.h"
#include "colors.h"

// static declarations in source file instead of header
static int songs_n;
static struct song *songs_arr = NULL;

static struct download_opts *dl_opts;

static struct song *parser_toml_table_to_song (const toml_table_t *tab);
static int parser_read_dl_opts(toml_table_t *root_tab);

int parser_parse_song_file (const char *file_name)
{
    // Try to open the file
    FILE *fp;
    fp = fopen(file_name, "r");
    if (!fp) {
        fprintf(stderr,
                COLOR_RED "Error: Failed to open file %s: %s"
                COLOR_DEFAULT "\n",
                file_name,
                strerror(errno));
        return -1;
    }

    // Try to parse the file
    char errbuf[200];
    toml_table_t *root_tab;
    root_tab = toml_parse_file(fp, errbuf, sizeof(errbuf));
    fclose(fp);
    if (!root_tab) {
        fprintf(stderr,
                COLOR_RED "Error: Failed to parse file %s: %s"
                COLOR_DEFAULT "\n",
                file_name,
                errbuf);
        return -1;
    }


    // go through each subtable of root_tab
    int table_members_n = toml_table_ntab(root_tab)
                        + toml_table_nkval(root_tab)
                        + toml_table_narr(root_tab);

    for (int i = 0; i < table_members_n; i++) {
        toml_table_t *tab = toml_table_in(root_tab,
                                        toml_key_in(root_tab, i));

        if (tab) {
            // convert toml table to song struct
            struct song *s = parser_toml_table_to_song(tab);
            if (!s) {
                return -1;
            }

            if (parser_check_song_required(s)){
                fprintf(stderr, 
                        COLOR_RED "Error: member %d of %s doesn't \
                        contain all necessary song information!"
                        COLOR_DEFAULT "\n",
                        i+1, file_name);
                song_free(s);
                return -1;
            }
            if (parser_add_song(s)) {
                song_free(s);
                return -1;
            }
        }
    }

    // get dl_opts
    if (parser_read_dl_opts(root_tab))
        return -1; // failed

    // free the root table and all its children
    toml_free(root_tab);

    return 0;
}

static int parser_read_dl_opts(toml_table_t *root_tab)
{
    dl_opts = malloc(sizeof(struct download_opts));
    if (!dl_opts)
        goto memerr;

    toml_datum_t format;
    toml_datum_t quality;

    /* get format */
    format = toml_string_in(root_tab, "format");
    if (format.ok) {
        dl_opts->format = format.u.s;
    } else { // set default format
        dl_opts->format = malloc(sizeof(DL_OPTS_FORMAT_DEFAULT));
        if (!dl_opts->format)
            goto memerr;
        strcpy(dl_opts->format, DL_OPTS_FORMAT_DEFAULT);
    }

    /* get quality */
    quality = toml_int_in(root_tab, "quality");
    if (quality.ok)
        dl_opts->quality = quality.u.i;
    else // set default quality
        dl_opts->quality = DL_OPTS_QUALITY_DEFAULT;

    return 0;

memerr:
    if (dl_opts) {
        free(dl_opts);
        dl_opts = NULL;
    }
    perror(COLOR_RED "Error: Failed to allocate memory for download opts" COLOR_DEFAULT);
    return -1;
}

// TODO: rewrite with song_create
static struct song *parser_toml_table_to_song (const toml_table_t *tab)
{
    /* initialize variables for song_create()
     * NULL will result in an empty, NULL terminated string */
    char *title_res = NULL;
    char *download_link_res = NULL;
    char *artist_res = NULL;
    char *genre_res = NULL;
    char *album_res = NULL;
    int release_year_res = 0;

    /* predeclare toml_*_in() returned toml_datum_t's */
    toml_datum_t title;
    toml_datum_t download_link;
    toml_datum_t artist;
    toml_datum_t genre;
    toml_datum_t album;
    toml_datum_t release_year;

    // get title
    title = toml_string_in(tab, "title"); /* read title from toml table */
    if (title.ok)
        title_res = title.u.s;

    // get download link
    download_link = toml_string_in(tab, "url");
    if (download_link.ok)
        download_link_res = download_link.u.s;

    // get artist name
    artist = toml_string_in(tab, "artist");
    if (artist.ok)
       artist_res = artist.u.s;

    // get genre
    genre = toml_string_in(tab, "genre");
    if (genre.ok)
        genre_res = genre.u.s;

    // get album
    album = toml_string_in(tab, "album");
    if (album.ok)
        album_res = album.u.s;

    // get release year
    release_year = toml_int_in(tab, "year");
    if (release_year.ok)
        release_year_res = release_year.u.i;

    // return song on success
    return song_create(title_res,
                       download_link_res,
                       artist_res,
                       genre_res,
                       album_res,
                       release_year_res);
}


int parser_add_song (struct song *new_song)
{
    // Check if songs_arr is already allocated
    if (songs_arr == NULL) {
        songs_arr = malloc(sizeof(struct song));
        songs_n = 1;
    } else { // increase size of songs_arr to hold one more song
        songs_n++;
        songs_arr = realloc(songs_arr,
                            songs_n * sizeof(struct song));
    }
    if (!songs_arr) {
        perror(COLOR_RED "Failed to allocate memory for songs list" COLOR_DEFAULT);
        return -1;
    }
    memcpy(&songs_arr[songs_n-1], new_song, sizeof(struct song));
    free(new_song);
    return 0;
}


int parser_check_song_required (const struct song *check_song)
{
    if (!strlen(check_song->title) || !strlen(check_song->download_link))
        return -1;

    return 0;
}



void parser_free_all ()
{
    for (int i = 0; i < songs_n; i++) {
        song_free(&songs_arr[i]);
    }
    free(songs_arr);
    songs_arr = NULL;
    songs_n = 0;

    if (dl_opts) {
        if (dl_opts->format)
            free(dl_opts->format);
        free(dl_opts);
        dl_opts = NULL;
    }


}

int parser_get_songs_n()
{
    return songs_n;
}


struct song *parser_get_song_by_index (int index) 
{

    if (index >= 0 && index < songs_n)
        return &songs_arr[index];

    // index not in array; return NULL
    return NULL;

}

struct download_opts *parser_get_dl_opts ()
{

    if (dl_opts)
        return dl_opts;
    else /* this should probably never be the case */
        return NULL;

}
