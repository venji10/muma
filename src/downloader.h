/*
 *   Copyright (C) 2022 venji10 <venji10@riseup.net>
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef DOWNLOADER_H
#define DOWNLOADER_H

#include "song.h"

enum provider {PROVIDER_NONE, PROVIDER_YOUTUBE, PROVIDER_WEB};

struct download_opts {
    char *format; /* Can be: "aac" "flac" "mp3" "m4a" "opus" "vorbis" "wav" */
    int quality; /* Can be: 0-9 | 0 = best | 9 = worst */
};

/* downloads the song pointed to by *s
 * and uses the download options *dl_opts
 * returns 0 on success, -1 otherwise */
int downloader_download_song (const struct song *s, const struct download_opts *dl_opts);

/* determines and returns a provider by checking
 * the download link for common patterns */
int downloader_get_provider (const struct song *s);

/* returns 0 if curl initializes successfully, -1 otherwise */
int downloader_curl_init();

/* should be called on program exit */
void downloader_free ();

#endif /* DOWNLOADER_H */
