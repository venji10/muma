/*
 *   Copyright (C) 2022 venji10 <venji10@riseup.net>
 *
 *   actions.c: responsible for providing a simplified way for accessing
 *   the program's actions.
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdio.h>

#include "actions.h"

#include "parser.h"
#include "downloader.h"
#include "creator.h"
#include "colors.h"

int actions_run_action (int action, const char *file_name, const char *action_arg)
{
    switch (action) {
    
        case ACTION_DOWNLOAD:
            return actions_download(file_name);

        case ACTION_CREATE:
            return actions_create(file_name);

        case ACTION_YT_SEARCH:
            return actions_yt_search(action_arg);

        case ACTION_YT_SEARCH_CREATE:
            return actions_yt_search_create(file_name, action_arg);

        case ACTION_YT_PLAYLIST_TO_LOCAL:
            return actions_yt_playlist_to_local(file_name, action_arg);

        default: /* no valid action provided */
            fprintf(stderr,
                    COLOR_RED "Error: no valid action argument provided! - see --help"
                    COLOR_DEFAULT "\n");
            return -1;
    }
}

int actions_download (const char *file_name)
{
    printf(COLOR_CYAN
            "Trying to download songs from file \"%s\"\n"
            COLOR_DEFAULT "\n",
            file_name);

    if (parser_parse_song_file(file_name)) {
        fprintf(stderr,
                COLOR_RED "Error: Failed to parse TOML file %s"
                COLOR_DEFAULT "\n",
                file_name);
        goto error_out;
    }

    struct download_opts *dl_opts = parser_get_dl_opts();
    if (!dl_opts) { // again, this shouldn't be possible
        fprintf(stderr, 
                COLOR_RED "Error: dl_opts does not exist"
                COLOR_DEFAULT "\n");
        goto error_out;
    }

    struct song *s;
    int ret;

    if (downloader_curl_init()) {
        fprintf(stderr,
                COLOR_RED "Error: Failed to initialize libcurl!"
                COLOR_DEFAULT "\n");
        goto error_out;
    }

    for (int i = 0; i < parser_get_songs_n(); i++) {
        s = parser_get_song_by_index(i);
        if (s) {
            ret = downloader_download_song(s, dl_opts);
            if (ret) {
                fprintf(stderr,
                        COLOR_RED "Error: Failed to download song \"%s\""
                        COLOR_DEFAULT "\n",
                        s->title);
                goto error_out;
            }
        } else {
            fprintf(stderr,
                    COLOR_RED "Error: Failed to get song from parser!"
                    COLOR_DEFAULT "\n");
            goto error_out;
        }
    }

    downloader_free();
    parser_free_all();
    return 0;

error_out:
    downloader_free();
    parser_free_all();
    return -1;

}

int actions_create (const char *file_name)
{

    return creator_interactive_create(file_name);

}

int actions_yt_search (const char *term)
{

    return creator_yt_search(term);

}

int actions_yt_search_create (const char *file_name, const char *term)
{

    return creator_yt_search_create(file_name, term);

}

int actions_yt_playlist_to_local (const char *file_name, const char *url)
{

    return creator_yt_playlist_to_local(file_name, url);

}

