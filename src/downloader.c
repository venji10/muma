/*
 *   Copyright (C) 2022 venji10 <venji10@riseup.net>
 *
 *   downloader.c: Provides functions for downloading songs from various
 *   providers in the desired format.
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#include <sys/stat.h>
#include <sys/wait.h> 

#include <curl/curl.h>

#include "downloader.h"
#include "audiomod.h"
#include "ytdl.h"

#include "colors.h"

static char *ytdl_args[] = {"--ignore-config",
                    "--no-playlist",
                    "--extract-audio"};


static CURL *curl = NULL;
static CURLcode curl_result;
static char curl_errbuf[CURL_ERROR_SIZE];

/* returns 0 on successful download; returns -1 on error */
int downloader_download_ytdl (const struct song *s, const struct download_opts *dl_opts);
int downloader_download_web (const struct song *s, const struct download_opts *dl_opts);

int downloader_download_song(const struct song *s, const struct download_opts *dl_opts) {

    printf(COLOR_GREEN "Trying to download song \"%s\"" COLOR_DEFAULT "\n", s->title);

    int provider = downloader_get_provider(s);

    switch (provider) {
    
        case PROVIDER_YOUTUBE:
            return downloader_download_ytdl(s, dl_opts);

        case PROVIDER_WEB:
            return downloader_download_web(s, dl_opts);
            break;
    }

    return -1;

}


int downloader_get_provider (const struct song *s) {

    if (strstr(s->download_link, "https://youtu") ||
            strstr(s->download_link, "http://youtu") ||
            strstr(s->download_link, "www.youtu"))
        return PROVIDER_YOUTUBE;
    else
        return PROVIDER_WEB;

}

int downloader_download_ytdl(const struct song *s, const struct download_opts *dl_opts)
{
    if (ytdl_init_path_check_installed()) {
        fprintf(stderr,
                COLOR_RED
                "Error: youtube-dl is not installed / was not found!"
                COLOR_DEFAULT "\n");
        return -1;
    }

    /* don't check return value, we successfully initialized already */
    char *ytdl_path = ytdl_get_path();

    char tmp_file_ext[] = "tmpx";

    char *output_file_name;
    char *dl_quality;

    output_file_name = malloc(strlen("-.") /* non changing chars */
                                    + strlen(s->artist)
                                    + strlen(s->title)
                                    + strlen(tmp_file_ext)
                                    + 1);

    if (!output_file_name) {
        perror(COLOR_RED "Error: Failed to allocate memory for output file name"
               COLOR_DEFAULT);
        return -1;
    }

    sprintf(output_file_name,
            "%s-%s.%s",
            s->artist,
            s->title,
            tmp_file_ext);

    dl_quality = malloc(strlen("-xxxx") +1);

    if (!dl_quality) {
        free(output_file_name);
        perror(COLOR_RED "Error: Failed to allocate memory for download quality string"
               COLOR_DEFAULT);
        return -1;
    }

    snprintf(dl_quality, strlen("-xxxx") +1, "%d", dl_opts->quality);

    char *ytdl_cmd[] = {ytdl_path,
                        ytdl_args[0], ytdl_args[1], ytdl_args[2],
                        "--audio-format", dl_opts->format,
                        "--audio-quality", dl_quality,
                        "-o", output_file_name,
                        s->download_link,
                        NULL
    };

    pid_t child_pid = fork();
    if (!child_pid) { /* child process */
        execvp(ytdl_cmd[0], ytdl_cmd);
        perror(COLOR_RED "Error: Failed to run youtube-dl" COLOR_DEFAULT);
        exit(-1);
    }

    int child_status;
    waitpid(child_pid, &child_status, 0);

    int exit_code = WEXITSTATUS(child_status);
    if (exit_code) {
        fprintf(stderr,
                COLOR_RED "Error: Failed to download song %s"
                COLOR_DEFAULT "\n",
                s->title);
        exit_code = -1;
        goto exit;
    }

    /* update extension of file from tmpx to extension of result */
    output_file_name[strlen(output_file_name)-strlen(tmp_file_ext)] = '\0';

    /* combine file path and demanded format extension */
    if (!strcmp(dl_opts->format, "aac")) {
        strcat(output_file_name, "aac");

    } else if (!strcmp(dl_opts->format, "flac")) {
        strcat(output_file_name, "flac");

    } else if (!strcmp(dl_opts->format, "mp3")) {
        strcat(output_file_name, "mp3");

    } else if (!strcmp(dl_opts->format, "m4a")) {
        strcat(output_file_name, "m4a");

    } else if (!strcmp(dl_opts->format, "opus")) {
        strcat(output_file_name, "opus");

    } else if (!strcmp(dl_opts->format, "vorbis")) {
        strcat(output_file_name, "ogg");

    } else if (!strcmp(dl_opts->format, "wav")) {
        strcat(output_file_name, "wav");

    } else {
        fprintf(stderr,
                COLOR_RED "Error: Unknown format: %s"
                COLOR_DEFAULT "\n",
                dl_opts->format);
        exit_code = -1;
        goto exit;
    }

    /* add metadata to file */
    if (audiomod_add_metadata(s, output_file_name))
        exit_code = -1; /* adding metadata failed */

    printf(COLOR_GREEN
        "Successfully downloaded song \"%s\"\n" 
        COLOR_DEFAULT "\n",
        s->title);

exit:
    free(output_file_name);
    free(dl_quality);
    return exit_code;

}

int downloader_curl_init()
{
    curl_global_init(CURL_GLOBAL_ALL);
    curl = curl_easy_init();
    if (curl) {
        curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, NULL);
        curl_easy_setopt(curl, CURLOPT_NOPROGRESS, 0L);
        curl_easy_setopt(curl, CURLOPT_FAILONERROR, 1L);
        curl_easy_setopt(curl, CURLOPT_ERRORBUFFER, &curl_errbuf);
        return 0;
    } else {
        return -1;
    }
}

int downloader_download_web (const struct song *s, const struct download_opts *dl_opts)
{
    if (!curl) {
        fprintf(stderr,
                COLOR_RED "Error: curl not initialized!"
                COLOR_DEFAULT "\n");
        return -1;
    }

    /* default exit code; remains unchanged if nothing fails */
    int exit_code = 0;

    /* declare various variables */
    char *file_name;
    FILE *file_dl;
    char *convert_out_file_name = NULL;


    file_name = malloc(strlen(s->artist)
                    + sizeof("-")
                    + strlen(s->title)
                    + 1);

    if (!file_name) {
        perror(COLOR_RED "Error: Failed to allocate memory for file name" COLOR_DEFAULT);
        return -1;
    }

    /* assemble file_name */
    sprintf(file_name, "%s-%s", s->artist, s->title);

    /* check if file already exists */
    struct stat file_stat;
    if (!stat(file_name, &file_stat))
        goto already_downloaded;

    file_dl = fopen(file_name, "wb");
    if (!file_dl) {
        perror(COLOR_RED "Failed to open target file" COLOR_DEFAULT);
        exit_code = -1;
        goto exit;
    }

    curl_easy_setopt(curl, CURLOPT_URL, s->download_link);
    curl_easy_setopt(curl, CURLOPT_WRITEDATA, file_dl);

    curl_result = curl_easy_perform(curl);

    /* clean up */
    fclose(file_dl);

    if (curl_result != CURLE_OK) {
        fprintf(stderr,
                COLOR_RED
                "Error downloading title %s - libcurl error code: %d"
                COLOR_DEFAULT "\n",
                s->title, curl_result);
        if(strlen(curl_errbuf))
            fprintf(stderr,
                    COLOR_RED "Error message: %s"
                    COLOR_DEFAULT "\n",
                    curl_errbuf);
        return -1;
    }

    convert_out_file_name = audiomod_convert_audio(file_name, dl_opts);
    if (!convert_out_file_name){
        exit_code = -1; /* converting audio failed */
        goto exit;
    }
        
    /* add metadata to file */
    if (audiomod_add_metadata(s, convert_out_file_name)) {
        exit_code = -1; /* adding metadata failed */
        goto exit;
    }

    printf(COLOR_GREEN
        "Successfully downloaded song \"%s\"\n"
        COLOR_DEFAULT "\n",
        s->title);

exit:
    if (remove(file_name))
        fprintf(stderr,
                COLOR_RED "Error: Failed to delete tmp file %s"
                COLOR_DEFAULT "\n",
                file_name);
    if (convert_out_file_name)
        free(convert_out_file_name);
    free(file_name);
    return exit_code;

already_downloaded:
    free(file_name);
    printf(COLOR_CYAN "Song \"%s\" already downloaded!" COLOR_DEFAULT "\n", s->title);
    return 0;
}

void downloader_free ()
{
    ytdl_free();

    if (curl) {
        curl_easy_cleanup(curl);
        curl = NULL;
    }
    curl_global_cleanup();

    audiomod_free();

}
