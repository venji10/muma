/*
 *   Copyright (C) 2022 venji10 <venji10@riseup.net>
 *
 *   song.c: provides functions related to the song struct.
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>

#include "song.h"

#include "colors.h"

void song_free (struct song *s)
{
    if (s->title) {
        free(s->title);
        s->title = NULL;
    }

    if (s->download_link) {
        free(s->download_link);
        s->download_link = NULL;
    }

    if (s->artist) {
        free(s->artist);
        s->artist = NULL;
    }

    if (s->genre) {
        free(s->genre);
        s->genre = NULL;
    }

    if (s->album) {
        free(s->album);
        s->album = NULL;
    }
}

struct song *song_create (char *title, char *download_link, char *artist,
                                char *genre, char *album, int release_year)

{

    /* allocate memory for song struct */
    struct song *s = malloc(sizeof(struct song));
    if (!s)
        goto memerr;

    /* title */
    if (title) {
        s->title = title;
    } else {
        s->title = malloc(sizeof(char));
        if (!s->title)
            goto memerr;
        s->title[0] = '\0';
    }

    /* download link */
    if (download_link) {
        s->download_link = download_link;
    } else {
        s->download_link = malloc(sizeof(char));
        if (!s->download_link)
            goto memerr;
        s->download_link[0] = '\0';
    }

    /* artist */
    if (artist) {
        s->artist = artist;
    } else {
        s->artist = malloc(sizeof(char));
        if (!s->artist)
            goto memerr;
        s->artist[0] = '\0';
    }

    /* genre */
    if (genre) {
        s->genre = genre;
    } else {
        s->genre = malloc(sizeof(char));
        if (!s->genre)
            goto memerr;
        s->genre[0] = '\0';
    }

    /* album */
    if (album) {
        s->album = album;
    } else {
        s->album = malloc(sizeof(char));
        if (!s->album)
            goto memerr;
        s->album[0] = '\0';
    }

    /* release year */
    if (release_year)
        s->release_year = release_year;
    else
        s->release_year = 0;

    /* return the song struct */
    return s;

memerr:
    perror(COLOR_RED "Failed to allocate memory for song!" COLOR_DEFAULT);
    song_free(s);
    if (s)
        free(s);
    return NULL;
}
