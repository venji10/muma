/*
 *   Copyright (C) 2022 venji10 <venji10@riseup.net>
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef SONG_H
#define SONG_H

struct song {
    char *type; //TODO: add vars and support for split songs
    char *title; // required
    char *download_link; // required
    char *artist;
    char *genre;
    char *album;
    int release_year;
};

/* frees the content of a song struct,
 * but not the struct itself */
void song_free (struct song *s);

/* returns a song struct pointer with values from the arguments
 * returns NULL on failure */
struct song * song_create (char *title, char *download_link, char *artist,
                        char *genre, char *album, int release_year);

#endif /* SONG_H */
