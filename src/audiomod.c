/*
 *   Copyright (C) 2022 venji10 <venji10@riseup.net>
 *
 *   audiomod.c: responsible for audio modifications, like format
 *   conversion and adding metadata to music files.
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#include <sys/stat.h>
#include <sys/wait.h>

#include <tag_c.h>

#include "audiomod.h"
#include "downloader.h"

#include "colors.h"

/* see FFmpeg man page for other loglevels */
#define AUDIOMOD_FFMPEG_LOGLEVEL "warning"

char *ffmpeg_path = NULL;
int is_ffmpeg_path_env = 0;

int audiomod_is_ffmpeg_installed ()
{
    if (!ffmpeg_path) {
        /* read FFmpeg env variable */
        ffmpeg_path = getenv("FFMPEG_PATH");
        if (!ffmpeg_path) { /* use default path */
            ffmpeg_path = malloc(sizeof(FFMPEG_PATH_DEFAULT));
            if (!ffmpeg_path) {
                perror(COLOR_RED "Error: Failed to allocate memory for FFmpeg path"
                       COLOR_DEFAULT);
                return -1;
            }
            strcpy(ffmpeg_path, FFMPEG_PATH_DEFAULT);
        } else {
            is_ffmpeg_path_env = 1; /* env var doesn't need to be freed */
        }

        /* check if FFmpeg is at the given path */
        struct stat ffmpeg_stat;
        return stat(ffmpeg_path, &ffmpeg_stat);
    }

    return 0; /* already checked in previous run */

}


char *audiomod_convert_audio (char *file_name, const struct download_opts *dl_opts)
{

    if (audiomod_is_ffmpeg_installed()) {
        fprintf(stderr,
                COLOR_RED "Error: FFmpeg is not installed / was not found!"
                COLOR_DEFAULT "\n");
        return NULL;
    }

    printf(COLOR_CYAN
        "Converting song to format \"%s\""
        COLOR_DEFAULT "\n",
        dl_opts->format);

    char *out_file_name = malloc(strlen(file_name) 
                                + strlen(".xxxx") /* longest file extension = 4 chars */
                                + 1); /* \0 */

    if (!out_file_name) {
        perror(COLOR_RED "Error: Failed to allocate memory for file name" COLOR_DEFAULT);
        return NULL;
    }

    /* combine file path and demanded format extension */
    if (!strcmp(dl_opts->format, "aac")) {
        sprintf(out_file_name, "%s.aac", file_name);

    } else if (!strcmp(dl_opts->format, "flac")) {
        sprintf(out_file_name, "%s.flac", file_name);

    } else if (!strcmp(dl_opts->format, "mp3")) {
        sprintf(out_file_name, "%s.mp3", file_name);

    } else if (!strcmp(dl_opts->format, "m4a")) {
        sprintf(out_file_name, "%s.m4a", file_name);

    } else if (!strcmp(dl_opts->format, "opus")) {
        sprintf(out_file_name, "%s.opus", file_name);

    } else if (!strcmp(dl_opts->format, "vorbis")) {
        sprintf(out_file_name, "%s.ogg", file_name);

    } else if (!strcmp(dl_opts->format, "wav")) {
        sprintf(out_file_name, "%s.wav", file_name);

    } else {
        fprintf(stderr, COLOR_RED "Error: Unknown format: %s"
                COLOR_DEFAULT "\n",
                dl_opts->format);
        goto error_out;
    }

    struct stat out_stat;
    if (!stat(out_file_name, &out_stat)) {
        printf(COLOR_YELLOW "File already exists!" COLOR_DEFAULT "\n");
        return out_file_name;
    }

    char *ffmpeg_cmd[] = {ffmpeg_path,
                    "-loglevel", AUDIOMOD_FFMPEG_LOGLEVEL,
                    "-i", file_name,
                    out_file_name,
                    "-n",
                    NULL};

    pid_t child_pid = fork();
    if (!child_pid) { /* child process */
        execvp(ffmpeg_cmd[0], ffmpeg_cmd);
        perror(COLOR_RED "Error: Failed to run FFmpeg" COLOR_DEFAULT);
        exit(-1);
    }

    int child_status;
    waitpid(child_pid, &child_status, 0);

    int exit_code = WEXITSTATUS(child_status);
    if (exit_code) {
        fprintf(stderr,
                COLOR_RED "Error: Failed to convert audio using FFmpeg"
                COLOR_DEFAULT "\n");
        goto error_out;
    }

    printf(COLOR_CYAN "Successfully converted song to format \"%s\""
        COLOR_DEFAULT "\n",
        dl_opts->format);
    return out_file_name;

error_out:
    free(out_file_name);
    return NULL;
}

int audiomod_add_metadata (const struct song *s, const char *file_name)
{
    printf(COLOR_CYAN "Adding metadata to target file %s" COLOR_DEFAULT "\n", file_name);

    TagLib_File *file = NULL;
    TagLib_Tag *file_tag = NULL;

    file = taglib_file_new(file_name);
    if (!file) {
        fprintf(stderr,
                COLOR_RED
                "Error: Failed to open \"%s\" for adding metadata"
                COLOR_DEFAULT "\n",
                file_name);
        goto error_out;
    }

    file_tag = taglib_file_tag(file);

    if (!file_tag) {
        fprintf(stderr,
                COLOR_RED
                "Error: Failed to get metadata handle from file \"%s\""
                COLOR_DEFAULT "\n",
                file_name);
        goto error_out;
    
    }

    // set metadata
    taglib_tag_set_title(file_tag, s->title);
    taglib_tag_set_artist(file_tag, s->artist);
    taglib_tag_set_album(file_tag, s->album);
    taglib_tag_set_genre(file_tag, s->genre);
    taglib_tag_set_year(file_tag, s->release_year);

    // unneeded / unused metadata
    // taglib_tag_set_track(file_tag, 1);
    // taglib_tag_set_comment(file_tag, "This is a comment");

    // save to file
    taglib_file_save(file);

    taglib_tag_free_strings();
    taglib_file_free(file);

    printf(COLOR_CYAN "Successfully added metadata to target file" COLOR_DEFAULT "\n");

    return 0;

error_out:
    taglib_tag_free_strings();
    if (file)
        taglib_file_free(file);
    return -1;
}

void audiomod_free ()
{
    if (ffmpeg_path && !is_ffmpeg_path_env) {
        free(ffmpeg_path);
        ffmpeg_path = NULL; /* safe is safe */
    }
}
