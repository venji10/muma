/*
 *   Copyright (C) 2022 venji10 <venji10@riseup.net>
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef CREATOR_H
#define CREATOR_H

/* all functions in this header return 0 on success, -1 otherwise */

/* create a TOML file in interactive mode
 * file_name: path to the desired output file;
 * data will be appended if the file already exists */
int creator_interactive_create (const char *file_name);

/* prints the first 10 results from the YouTube search for term
 * TODO: add an argument for the amount of printed search results? */
int creator_yt_search (const char *term);

/* not implemented yet */
int creator_yt_search_create (const char *file_name, const char *term);

/* converts a YouTube playlist to a TOML file
 * file_name: the path to the desired output file;
 * data will be appended if the file already exists
 * url: URL to a YouTube playlist to be converted */
int creator_yt_playlist_to_local (const char *file_name, const char *url);

#endif /* CREATOR_H */
