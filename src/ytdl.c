/*
 *   Copyright (C) 2022 venji10 <venji10@riseup.net>
 *
 *   ytdl.c: provides common functions for getting the youtube-dl binary
 *   path for various youtube-dl based functions.
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>

#include "ytdl.h"

#include "colors.h"

static char *ytdl_path = NULL;
static int is_ytdl_path_invalid = 1;
static int is_ytdl_path_env = 0;

int ytdl_init_path_check_installed ()
{
    if (!ytdl_path) {
        /* read ytdl env variable */
        ytdl_path = getenv("YTDL_PATH");
        if (!ytdl_path) { /* use default path */
            ytdl_path = malloc(sizeof(YTDL_PATH_DEFAULT));
            if (!ytdl_path) {
                perror(COLOR_RED "Error: Failed to allocate memory for youtube-dl path"
                       COLOR_DEFAULT);
                return -1;
            }
            strcpy(ytdl_path, YTDL_PATH_DEFAULT);
        } else {
            is_ytdl_path_env = 1; /* env var doesn't need to be freed */
        }

        /* check if ytdl is at the given path */
        struct stat ytdl_stat;
        is_ytdl_path_invalid = stat(ytdl_path, &ytdl_stat);
        return is_ytdl_path_invalid;
    }

    return 0; /* already checked in previous run */
}

char *ytdl_get_path ()
{
    /* check, if path is valid */
    if (!is_ytdl_path_invalid)
        return ytdl_path;

    return NULL;
}

void ytdl_free ()
{
    if (!is_ytdl_path_env)
        free(ytdl_path);
    ytdl_path = NULL;
}

