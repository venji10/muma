/*
 *   Copyright (C) 2022 venji10 <venji10@riseup.net>
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef ACTIONS_H
#define ACTIONS_H

enum actions {ACTION_NONE,
            ACTION_DOWNLOAD,
            ACTION_CREATE,
            ACTION_YT_SEARCH,
            ACTION_YT_SEARCH_CREATE,
            ACTION_YT_PLAYLIST_TO_LOCAL
            };

/* all functions here return 0 on success, -1 otherwise */

int actions_run_action (int action, const char *file_name, const char *action_arg);

int actions_download (const char *file_name);
int actions_create (const char *file_name);
int actions_yt_search (const char *term);
int actions_yt_search_create (const char *file_name, const char *term);
int actions_yt_playlist_to_local (const char *file_name, const char *url);

#endif /* ACTIONS_H */
