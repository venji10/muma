/*
 *   Copyright (C) 2022 venji10 <venji10@riseup.net>
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef YTDL_H
#define YTDL_H

/* default path for youtube-dl if the YTDL_PATH env variable isn't set */
#define YTDL_PATH_DEFAULT "/usr/bin/youtube-dl"

/* returns 0 if youtube-dl is installed; returns -1 if not.
 * gets the path from the the YTDL_PATH env variable or sets the
 * path to YTDL_PATH_DEFAULT otherwise.
 * checks, if the path exists */
int ytdl_init_path_check_installed ();

/* returns a pointer to the ytdl path or NULL if not initialized */
char *ytdl_get_path ();

void ytdl_free ();

#endif /* YTDL_H */
