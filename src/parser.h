/*
 *   Copyright (C) 2022 venji10 <venji10@riseup.net>
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef PARSER_H
#define PARSER_H

#include "song.h"

/* default download options if not set in TOML */
#define DL_OPTS_FORMAT_DEFAULT "vorbis"
#define DL_OPTS_QUALITY_DEFAULT 0

/* These 3 functions all return 0 on success, -1 otherwise */
/* parses file_name and fills the local songs array and dl_opts
 * with the files content */
int parser_parse_song_file (const char *file_name);

/* adds a song to the local songs array and frees the supplied
 * pointer afterwards*/
int parser_add_song (struct song *new_song);

/* checks, if a song meets all requirements to be downloaded */
int parser_check_song_required (const struct song *check_song);


/* should be called on program exit;
 * frees the local songs array and the dl_opts */
void parser_free_all ();

/* returns the number of song structs in the local songs array */
int parser_get_songs_n ();

/* returns a pointer to the song at index or NULL on failure */
struct song *parser_get_song_by_index (int index);

/* returns the local dl_opts struct,
 * or NULL on failure */
struct download_opts *parser_get_dl_opts ();

#endif /* PARSER_H */
