/*
 *   Copyright (C) 2022 venji10 <venji10@riseup.net>
 *
 *   main.c: entry point to the program. Responsible for parsing and handling
 *   command line arguments.
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <getopt.h>

#include "actions.h"

#include "colors.h"

int is_root();

void print_version ();
void print_help ();

int main (int argc, char **argv)
{

    /* refuse to execute as root;
     * It is probably unsafe and there is no reason to do so */
    if (is_root()) {
        fprintf(stderr,
                COLOR_RED "Error: This program cannot be executed as root!"
                COLOR_DEFAULT "\n");
        return EXIT_FAILURE;
    }

    struct option options[] = {
        {"version",                 no_argument,        NULL, 'v'},
        {"help",                    no_argument,        NULL, 'h'},
        {"download",                no_argument,        NULL, 'd'},
        {"create",                  no_argument,        NULL, 'c'},
        {"yt-search",               required_argument,  NULL, 's'},
        {"yt-search-create",        required_argument,  NULL, 't'},
        {"yt-playlist-to-local",    required_argument,  NULL, 'p'},
        {"file",                    required_argument,  NULL, 'f'},
        {0, 0, 0, 0}
    };

    int action = ACTION_NONE;
    char *action_arg = NULL;
    char *file_name = NULL;

    /* is set to 0, if action doesnt require a file name (search for example) */
    int is_file_name_required = 1;

    int option_index = 0;
    int c;

    while (-1 != (c = getopt_long(argc, argv, "vhdcs:t:p:f:", options, &option_index))) {
        switch (c) {
            case 'v': /* print version */
                print_version();
                return EXIT_SUCCESS;

            case 'h': /* print help */
                print_help();
                return EXIT_SUCCESS;

            case 'd': /* download argument */
                if (!action)
                    action = ACTION_DOWNLOAD;
                else
                    goto error_multi_actions;
                break;

            case 'c': /* create argument */
                if (!action)
                    action = ACTION_CREATE;
                else
                    goto error_multi_actions;
                break;

            case 's': /* yt search argument */
                if (!action) {
                    action = ACTION_YT_SEARCH;
                    action_arg = optarg;
                    is_file_name_required = 0; /* file name not required here */
                } else {
                    goto error_multi_actions;
                }
                break;

            case 't': /* yt search create argument */
                if (!action) {
                    action = ACTION_YT_SEARCH_CREATE;
                    action_arg = optarg;
                } else {
                    goto error_multi_actions;
                }
                break;

            case 'p': /* yt playlist to local argument*/
                if (!action) {
                    action = ACTION_YT_PLAYLIST_TO_LOCAL;
                    action_arg = optarg;
                } else {
                    goto error_multi_actions;
                }
                break;

            case 'f': /* file argument */
                if (!file_name) {
                    file_name = optarg;
                } else {
                    fprintf(stderr,
                            COLOR_RED
                            "Error: cannot have multiple file arguments!\n\
                            Try 'muma --help' for more information."
                            COLOR_DEFAULT "\n");
                    return EXIT_FAILURE;
                }
                break;

            default:
                return EXIT_FAILURE;
        }
    }

    // error, if not enough arguments were provided
    if (!action) {
        fprintf(stderr, COLOR_RED "Error: No action provided!\n\
                Try 'muma --help' for more information."
                COLOR_DEFAULT "\n");
        return EXIT_FAILURE;
    }
    
    if (!file_name && is_file_name_required) {
        fprintf(stderr, COLOR_RED "Error: No file option provided!\n\
                Try 'muma --help' for more information."
                COLOR_DEFAULT "\n");
        return EXIT_FAILURE;
    }

    // run the selected action
    if (actions_run_action(action, file_name, action_arg))
        return EXIT_FAILURE;

    return EXIT_SUCCESS;

error_multi_actions:
    fprintf(stderr, COLOR_RED "Error: Cannot have more than one action argument!\n\
            Try 'muma --help' for more information."
            COLOR_DEFAULT "\n");
    return EXIT_FAILURE;
}

/* check if program is executed as root */
int is_root ()
{

    if (geteuid() == 0)
        return 1;
    else
        return 0;

}

void print_version()
{

    printf(COLOR_YELLOW "Music Manager (muma) " MUMA_VERSION "\n"
COLOR_CYAN
"Copyright (C) 2022 venji10 <venji10@riseup.net> \n\
License GPLv3+: GNU GPL version 3 or later <https://gnu.org/licenses/gpl.html>. \n\
This is free software: you are free to change and redistribute it. \n\
There is NO WARRANTY, to the extent permitted by law.\n\
\n"
COLOR_YELLOW
"Copyright of libraries/applications used in this program: \n"
COLOR_GREEN
"FFmpeg (https://ffmpeg.org): Licensed under the LGPLv2.1 (http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html) \n\
TagLib (https://taglib.org): Licensed under the LGPLv2.1 (http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html) \n\
tomlc99 (https://github.com/cktan/tomlc99): Licensed under the following terms: \n"
COLOR_DEFAULT
"MIT License \n\
\n\
Copyright (c) CK Tan \n\
https://github.com/cktan/tomlc99 \n\
\n\
Permission is hereby granted, free of charge, to any person obtaining a copy \n\
of this software and associated documentation files (the \"Software\"), to deal \n\
in the Software without restriction, including without limitation the rights \n\
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell \n\
copies of the Software, and to permit persons to whom the Software is \n\
furnished to do so, subject to the following conditions: \n\
\n\
The above copyright notice and this permission notice shall be included in all \n\
copies or substantial portions of the Software. \n\
\n\
THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR \n\
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, \n\
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE \n\
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER \n\
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, \n\
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE \n\
SOFTWARE. \n\
\n"
COLOR_GREEN
"libcurl (https://curl.se/libcurl/): Licensed under the following terms: \n"
COLOR_DEFAULT
"COPYRIGHT AND PERMISSION NOTICE \n\
\n\
Copyright (c) 1996 - 2022, Daniel Stenberg, daniel@haxx.se, and many contributors, see the THANKS file. \n\
\n\
All rights reserved. \n\
\n\
Permission to use, copy, modify, and distribute this software for any purpose with or without \n\
fee is hereby granted, provided that the above copyright notice and this permission notice appear \n\
in all copies. \n\
\n\
THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, \n\
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR \n\
PURPOSE AND NONINFRINGEMENT OF THIRD PARTY RIGHTS. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT \n\
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, \n\
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER \n\
DEALINGS IN THE SOFTWARE. \n\
\n\
Except as contained in this notice, the name of a copyright holder shall not be used in advertising \n\
or otherwise to promote the sale, use or other dealings in this Software without prior written \n\
authorization of the copyright holder.\n\
\n"
COLOR_GREEN
"youtube-dl (https://yt-dl.org/): Licensed under the Unlicense License (https://unlicense.org/)\n" COLOR_DEFAULT);

}

void print_help()
{

    printf("Usage: muma [ACTION] [FILE OPTION] \n\
Manage your offline music playlists. \n\
\n\
The following ACTION arguments are available: \n\
\n\
These ACTIONs don't require a FILE OPTION: \n\
  -s, --yt-search <term>            Search for <term> on YouTube; \n\
                                      Lets you get the (title, video id, length) of the first ten results \n\
\n\
These ACTIONs require a FILE OPTION: \n\
  -d, --download                    Download music from a TOML file in the current directory \n\
                                      FILE OPTION should be a path to a TOML song file \n\
  -c, --create                      Create a music TOML file interactively \n\
                                      FILE OPTION should be the path to the target file \n\
                                        Creates a new file if it doesn't exist or writes the output song \n\
                                            to the end of an already existing file \n\
  -p, --yt-playlist-to-local <url>  Converts a YouTube playlist provided as <url> into a song TOML file \n\
                                      FILE OPTION has the same functionality and behavior as in the create ACTION \n\
\n\
The following option can provide a FILE OPTION, if needed: \n\
  -f, --file <file path>            Provides a file path; needed for several ACTIONs \n\
\n\
Other options: \n\
  -h, --help                        Print this help message and exit \n\
  -v, --version                     Print version information and exit \n");

}
