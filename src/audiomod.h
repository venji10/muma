/*
 *   Copyright (C) 2022 venji10 <venji10@riseup.net>
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef AUDIOMOD_H
#define AUDIOMOD_H

#include "song.h"
#include "downloader.h"

/* default path for FFmpeg if the FFMPEG_PATH env variable isn't set */
#define FFMPEG_PATH_DEFAULT "/usr/bin/ffmpeg"

/* returns 0 if FFmpeg is installed,
 * -1 or another true value otherwise
 *  also checks the FFMPEG_PATH env variable */
int audiomod_is_ffmpeg_installed ();

/* returns new file name on success, NULL otherwise
 * file_name: should be the name of the file to be converted
 * file_ext: should be the extension of the current file without dot; example: "ogg"
 * or NULL if no extension
 * dl_opts: should contain the desired quality and format */
char *audiomod_convert_audio (char *file_name, const struct download_opts *dl_opts);

/* returns 0 on success, -1 on failure
 * s: song struct containing the metadata
 * file_name: name of the file, that should get the metadata */
int audiomod_add_metadata (const struct song *s, const char *file_name);

/* frees everything that has to be freed on program termination */
void audiomod_free ();

#endif /* AUDIOMOD_H */
