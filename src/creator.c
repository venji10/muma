/*
 *   Copyright (C) 2022 venji10 <venji10@riseup.net>
 *   
 *   creator.c: Provides all functions related to / for creating TOML song
 *   files.
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <time.h>

#include <sys/wait.h> 

#include "creator.h"

#include "ytdl.h"
#include "colors.h"

#define CREATOR_INTERACTIVE_STRLEN 256
#define CREATOR_INTERACTIVE_INTLEN 16

#define CREATOR_YTDL_BUFFERSIZE 64

int creator_interactive_create (const char *file_name)
{
    /* static memory allocation:
     * nobody should need more than 255 chars and
     * that simplifies the code */
    char title[CREATOR_INTERACTIVE_STRLEN] = "";
    char download_link[CREATOR_INTERACTIVE_STRLEN] = "";
    char artist[CREATOR_INTERACTIVE_STRLEN] = "";
    char genre[CREATOR_INTERACTIVE_STRLEN] = "";
    char album[CREATOR_INTERACTIVE_STRLEN] = "";
    char release_year[CREATOR_INTERACTIVE_INTLEN] = "";

    /* timestamp for unique for TOML table key*/
    time_t curr_time;
    curr_time = time(NULL);


    printf(COLOR_CYAN "Creating a TOML table for a song in interactive mode\n");
    printf("Writing output to end of file %s\n" COLOR_DEFAULT "\n", file_name);

    printf("Enter the title (*): ");
    while (!fgets(title, CREATOR_INTERACTIVE_STRLEN, stdin) || strlen(title) <= 2) {
        printf(COLOR_RED "This is required!" COLOR_DEFAULT "\n");
        printf("Enter the title (*): ");
    }

    printf("Enter the download link (*): ");
    while (!fgets(download_link, CREATOR_INTERACTIVE_STRLEN, stdin)
            || strlen(download_link) <= 2) {
        printf(COLOR_RED "This is required!" COLOR_DEFAULT "\n");
        printf("Enter the download link (*): ");
    }

    printf("Enter the artist's name: ");
    fgets(artist, CREATOR_INTERACTIVE_STRLEN, stdin);

    printf("Enter the title's genre: ");
    fgets(genre, CREATOR_INTERACTIVE_STRLEN, stdin);

    printf("Enter the title's album: ");
    fgets(album, CREATOR_INTERACTIVE_STRLEN, stdin);

    printf("Enter the release year: ");
    fgets(release_year, CREATOR_INTERACTIVE_INTLEN, stdin);

    /* remove newline from end of each input */
    title[strlen(title)-1] = '\0';
    download_link[strlen(download_link)-1] = '\0';
    artist[strlen(artist)-1] = '\0';
    genre[strlen(genre)-1] = '\0';
    album[strlen(album)-1] = '\0';
    release_year[strlen(release_year)-1] = '\0';

    /* write content to file */
    FILE *target_file = fopen(file_name, "a");
    if (!target_file) {
        perror(COLOR_RED "Error: Failed to open target file" COLOR_DEFAULT);
        return -1;
    }

    /* write table key */
    fprintf(target_file, "[title_gen%ld]\n", curr_time);

    /* write title */
    fprintf(target_file, "title = \"%s\"\n", title);
    /* write download link */
    fprintf(target_file, "url = \"%s\"\n", download_link);
    /* write artist */
    fprintf(target_file, "artist = \"%s\"\n", artist);
    /* write genre */
    fprintf(target_file, "genre = \"%s\"\n", genre);
    /* write album */
    fprintf(target_file, "album = \"%s\"\n", album);
    /* write release year */
    fprintf(target_file, "year = \"%s\"\n", release_year);
    /* blank line at end */
    fprintf(target_file, "\n");

    fclose(target_file);

    printf(COLOR_CYAN "Successfully wrote data to file \"%s\"\n", file_name);

    return 0;
}

int creator_yt_search (const char *term)
{
    if (ytdl_init_path_check_installed()) {
        fprintf(stderr,
                COLOR_RED "Error: youtube-dl is not installed / was not found!"
                COLOR_DEFAULT "\n");
        return -1;
    }

    /* don't check return value, we successfully initialized already */
    char *ytdl_path = ytdl_get_path();

    char *ytsearch_arg = malloc(strlen("ytsearch10:\"\"") + strlen(term) + 1);
    if (!ytsearch_arg) {
        perror(COLOR_RED "Error: Failed to allocate memory for ytsearch argument"
               COLOR_DEFAULT);
        ytdl_free();
        return -1;
    }

    sprintf(ytsearch_arg, "ytsearch10:\"%s\"", term);

    char *ytdl_cmd[] = {ytdl_path,
                        ytsearch_arg,
                        "--get-title",
                        "--get-duration",
                        "--get-id",
                        NULL
    };

    printf(COLOR_GREEN "Searching YouTube for %s\n" COLOR_DEFAULT "\n", term);
    printf(COLOR_CYAN "1. Video title\n");
    printf("2. Video id: Use it for getting the YouTube URL\
-> https://youtu.be/watch?v=<videoid>\n");
    printf("3. Video length" COLOR_DEFAULT "\n");

    printf(COLOR_BLUE "Getting data using youtube-dl..." COLOR_DEFAULT "\n");
    pid_t child_pid = fork();
    if (!child_pid) { /* child process */
        execvp(ytdl_cmd[0], ytdl_cmd);
        perror(COLOR_RED "Error: Failed to run youtube-dl" COLOR_DEFAULT);
        exit(-1);
    }

    int child_status;
    waitpid(child_pid, &child_status, 0);

    int exit_code = WEXITSTATUS(child_status);
    if (exit_code) {
        fprintf(stderr, COLOR_RED "Error: youtube-dl failed" COLOR_DEFAULT "\n");
        exit_code = -1;
        goto error_out;
    }

    printf(COLOR_BLUE "Youtube-dl exited successfully!\n");

error_out:
    free(ytsearch_arg);
    ytdl_free();
    return exit_code;

}

int creator_yt_search_create (const char *file_name, const char *term)
{

    /* print this in order to suppress related compiler warings */
    printf("file name: \"%s\"\n", file_name);
    printf("search term: \"%s\"\n", term);
    printf(COLOR_RED "Error: Not implemented yet!" COLOR_DEFAULT "\n");
    return -1;

}

int creator_yt_playlist_to_local (const char *file_name, const char *url)
{

    /* check and init ytdl first */
    if (ytdl_init_path_check_installed()) {
        fprintf(stderr,
                COLOR_RED "Error: youtube-dl is not installed / was not found!"
                COLOR_DEFAULT "\n");
        return -1;
    }

    printf(COLOR_CYAN "Converting YouTube playlist \"%s\" to TOML tables.\n", url);
    printf("Target file: %s\n" COLOR_DEFAULT "\n", file_name);

    /* don't check return value, we successfully initialized already */
    char *ytdl_path = ytdl_get_path();

    /* try to open target file */
    FILE *target_file = fopen(file_name, "a");

    if (!target_file) {
        perror(COLOR_RED "Error: Failed to open target file" COLOR_DEFAULT);
        return -1;
    }

    int exit_code = 0;

    /* predeclare certain variables used/freed in error_out */
    /* 'run ytdl, get output' related declarations */
    char *ytdl_cmd = NULL;
    char *ytdl_out = NULL;
    /* 'parse ytdl_out' related declarations */
    int songs_n = 0;
    char **title_arr = NULL;
    char **yt_id_arr = NULL;


    /* run youtube-dl, get output START */
    ytdl_cmd = malloc(strlen(ytdl_path)
                        + strlen(" ")
                        + strlen(url)
                        + strlen(" --get-title")
                        + strlen(" --get-id")
                        + 1);

    if (!ytdl_cmd) {
        perror(COLOR_RED "Error: Failed to allocate memory for youtube-dl command"
               COLOR_DEFAULT);
        exit_code = -1;
        goto error_out;
    }

    sprintf(ytdl_cmd, "%s %s --get-title --get-id", ytdl_path, url);

    ytdl_out = malloc(sizeof(char));
    if (!ytdl_out) {
        perror(COLOR_RED "Error: Failed to allocate memory for youtube-dl output"
               COLOR_DEFAULT);
        exit_code = -1;
        goto error_out;
    }
    ytdl_out[0] = '\0'; /* make this a NULL terminated string */
    int ytdl_out_realloc_n = 0;

    printf(COLOR_BLUE "Getting data using youtube-dl..." COLOR_DEFAULT "\n");
    char buffer[CREATOR_YTDL_BUFFERSIZE];
    FILE *pipe = popen(ytdl_cmd, "r");

    while (!feof(pipe)) {
        if (fgets(buffer, CREATOR_YTDL_BUFFERSIZE, pipe)) {

            ytdl_out_realloc_n++;
            ytdl_out = realloc(ytdl_out, 
                            sizeof(char) * (ytdl_out_realloc_n * CREATOR_YTDL_BUFFERSIZE)
                            + 1);
            if (!ytdl_out) {
                perror(COLOR_RED
                       "Error: Failed to reallocate memory for youtube-dl output"
                       COLOR_DEFAULT);
                pclose(pipe);
                exit_code = -1;
                goto error_out;
            }

            strcat(ytdl_out, buffer);
        }
    }

    /* close pipe and check exit status */
    if (WEXITSTATUS(pclose(pipe))) {
        fprintf(stderr, COLOR_RED "Error: youtube-dl command failed" COLOR_DEFAULT "\n");
        exit_code = -1;
        goto error_out;
    }
    printf(COLOR_BLUE "Youtube-dl exited successfully!\n" COLOR_DEFAULT "\n");
    /* run youtube-dl, get output END */

    /* parse ytdl_out START */

    /* get line count */
    int line_n = 0;
    for (unsigned long i = 0; i < strlen(ytdl_out); i++) {
        if (ytdl_out[i] == '\n')
            line_n++;
    }

    /* each song has two lines in output */
    songs_n = line_n / 2;

    /* setup arrays */
    title_arr = malloc(sizeof(char*) * songs_n);
    if (!title_arr) {
        perror(COLOR_RED "Error: Failed to allocate memory for title array"
               COLOR_DEFAULT);
        exit_code = -1;
        goto error_out;
    }
    yt_id_arr = malloc(sizeof(char*) * songs_n);
    if (!yt_id_arr) {
        perror(COLOR_RED "Error: Failed to allocate memory for YouTube URL array"
               COLOR_DEFAULT);
        exit_code = -1;
        goto error_out;
    }

    /* put arrays into another array in order to simplify the loop */
    char **songs[2] = {title_arr, yt_id_arr};

    /* initially allocate strings in array */
    for (int i = 0; i < songs_n; i++) {
        songs[0][i] = malloc(sizeof(char));
        if (!songs[0][i]) {
            perror(COLOR_RED "Error: Failed to allocate memory for initial song string"
                   COLOR_DEFAULT);
            exit_code = -1;
            goto error_out;
        }
        songs[1][i] = malloc(sizeof(char));
        if (!songs[1][i]) {
            perror(COLOR_RED "Error: Failed to allocate memory for initial song string"
                   COLOR_DEFAULT);
            exit_code = -1;
            goto error_out;
        }
    }

    /* read all chars from ytdl_out and put them into the correct array */
    int curr_song = 0;
    int line_len = 0;
    int line_type = 0; /* 0 = title; 1 = URL */

    for (unsigned long i = 0; i < strlen(ytdl_out); i++) {
        if (ytdl_out[i] == '\n') { /* terminate string and switch line type */
            songs[line_type][curr_song] = realloc(songs[line_type][curr_song],
                                                sizeof(char) * (line_len + 1));
            if (!songs[line_type][curr_song]) {
                perror(COLOR_RED "Error: Failed to reallocate memory for song"
                       COLOR_DEFAULT);
                exit_code = -1;
                goto error_out;
            }
            songs[line_type][curr_song][line_len] = '\0';
            line_type = (line_type) ? 0 : 1;
            line_len = 0;
            if (line_type == 0) /* new song starts in new line */
                curr_song++;
            continue;
        }
        /* add char to array */
        songs[line_type][curr_song] = realloc(songs[line_type][curr_song],
                                            sizeof(char) * (line_len + 1));
        if (!songs[line_type][curr_song]) {
            perror(COLOR_RED "Error: Failed to reallocate memory for song"
                   COLOR_DEFAULT);
            exit_code = -1;
            goto error_out;
        }
        songs[line_type][curr_song][line_len] = ytdl_out[i];
        line_len++;
    }

    /* parse ytdl_out END */

    /* write video data to the TOML file */
    printf(COLOR_CYAN "Writing extracted data to TOML file...\n");

    for (int i = 0; i < songs_n; i++) {
        fprintf(target_file, "[title%d]\n", i);
        fprintf(target_file, "title = \"%s\"\n", songs[0][i]);
        fprintf(target_file, "artist = \"\"\n");
        fprintf(target_file, "year = 0\n");
        fprintf(target_file, "genre = \"\"\n");
        fprintf(target_file, "url = \"https://youtu.be/watch?v=%s\"\n", songs[1][i]);
        fprintf(target_file, "\n");
    }

    printf("Successfully wrote data to TOML file!" COLOR_DEFAULT "\n");

error_out:
    if (ytdl_cmd)
        free(ytdl_cmd);
    if (ytdl_out)
        free(ytdl_out);
    /* free arrays */
    for (int i = 0; i < songs_n; i++) {
        /* Video title */
        if (songs[0][i])
            free(songs[0][i]);
        else
            break;

        /* Video id */
        if (songs[1][i])
            free(songs[1][i]);
        else
            break;
    }
    if (title_arr)
        free(title_arr);
    if (yt_id_arr)
        free(yt_id_arr);
    fclose(target_file);

    ytdl_free();

    return exit_code;
}
