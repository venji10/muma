# Muma
[![PRs Welcome](https://img.shields.io/badge/PRs-welcome-brightgreen.svg?style=flat-square)](https://makeapullrequest.com) 
[![GPLv3 license](https://img.shields.io/badge/License-GPLv3-blue.svg)](./LICENSE)

Muma is a tool for downloading and managing music from various sources.
It is intended to give users more control over their music and also the freedom to share downloadable playlists.
See [IDEA.md](./IDEA.md) for more information.

Music can be defined in [TOML](https://toml.io/en/) files and then be downloaded using muma.
Muma also provides functions for creating TOML files.
See [Usage](#usage) for more information.

## Contents
- [TODO](#todo)
- [Usage](#usage)
    - [Define music in TOML](#define-music-in-toml)
    - [Create a TOML file in interactive mode](#create-a-toml-file-in-interactive-mode)
    - [Search YouTube for a song](#search-youtube-for-a-song)
    - [Search YouTube for a song and create a TOML file](#search-youtube-for-a-song-and-create-a-toml-file)
    - [Convert a YouTube playlist to TOML](#convert-a-youtube-playlist-to-toml)
    - [Environment variables](#environment-variables)
- [Bugs](#bugs)
- [Build instructions](#build-instructions)
- [Developing](#developing)
- [Dependecies](#dependencies)
- [License](#license)

## TODO
- [ ] Unimplemented actions
    - [x] Implement 'download from TOML file' option
        - [x] basic download functionality (youtube-dl, web with libcurl)
        - [x] Add metadata to downloaded songs (using TagLib)
        - [x] Convert song to requested format (using FFmpeg)
    - [x] Implement 'interactive TOML create' option
    - [x] Implement 'YouTube search' option
    - [ ] Implement 'YouTube search and create a TOML file' option (medium)
    - [x] Implement 'convert a YouTube playlist to TOML' option
- [ ] Add timestamp song splitting support (medium)
- [x] Create a man page
- [x] Add documentation and examples
- [x] Add all licenses to man page
- [x] Add small explanation of its content / tasks for each source file
- [x] Installation using Makefile
- [x] Add print statements about what the program is currently doing / Colored print statements
- [x] Make global declarations static, if better
- [x] Check line length in code
- [x] Common ytdl path header
- [x] Move source files into seperate src folder
- [ ] Parse song information from Spotify or other providers for 'YouTube playlist to TOML' (hard)
- [x] Text color stays changed after program exit in some terminal emulators - Fix that (easy)

## Usage
### Define music in TOML
Music can be defined in [TOML](https://toml.io/en/) files.
There are two global keys available, which have to be defined directly in the root table,
outside of any subtables. They are used for all songs in the file.
| key       | value   | type      | description                                                                                                                                                  | default value |
|-----------|---------|-----------|--------------------------------------------------------------------------------------------------------------------------------------------------------------|---------------|
| `format`  | string  | optional  | Possible  values:  `"aac"`,  `"flac"`, `"mp3"`, `"m4a"`, `"opus"`, `"vorbis"`, `"wav"` Allows you to select an audio format, used for all songs in the file. | `"vorbis"`    |
| `quality` | integer | optional  | Possible values: 0-9 Sets the audio quality, only relevant for songs from YouTube.  0 = best quality; 9 = worst quality                                      | 0             |

Songs are defined in subtables. The table keys can be anything, but they have to be unique in the file. They can have the following keys:
| key      | value   | type      | description                                                                                                                              |
|----------|---------|-----------|------------------------------------------------------------------------------------------------------------------------------------------|
| `title`  | string  | required  | Sets the title - Used for metadata and the download's file name.                                                                         |
| `url`    | string  | required  | Sets the download link for the song. Must either be a YouTube link or a link to an audio file on a server or a single HTML audio player. |
| `artist` | string  | optional  | Sets the song's artist name - Used for metadata and the download's file name.                                                            |
| `album`  | string  | optional  | Sets the song's containing album. Used for metadata.                                                                                     |
| `genre`  | string  | optional  | Sets the song's genre. Used for metadata.                                                                                                |
| `year`   | integer | optional  | Sets the song's release year. Used for metadata.                                                                                         |

#### Example:
```
# Example of a downloadable song file
format = "opus"
quality = 0

[someuniquekeyforsong245]
title = "TestVideo"
artist = "PhilippHagemeister"
year = 2012
genre = "test"
album = "testalbum"
url = "https://youtu.be/watch?v=BaW_jenozKc"

[simplesong123]
title = "BackToAdventure"
url = "https://mp3l.jamendo.com/?trackid=252222&format=ogg2"
```
### Download music from a TOML file
Use the `--download` or `-d` option.

Provide a path to the file to be downloaded from with `--file` or `-f` `<path/to/file>`.
#### Example
`muma --download --file ~/Music/myplaylist.toml`
### Create a TOML file in interactive mode
Use the `--create` or `-c` option.

Provide a path to the desired output file with `--file` or `-f` `<path/to/file>`.

The output will be appended to the file, if the file already exists.
#### Example
`muma --create --file ~/Music/myplaylist.toml`
### Search YouTube for a song
Use the `--search` or `-s` option.

Provide the search term as the option's argument.
#### Example
`muma --search "free software song"`
### Search YouTube for a song and create a TOML file
Not implemented yet.
### Convert a YouTube playlist to TOML
Use the `--yt-playlist-to-local` or `-p` option.

Provide the playlist's YouTube link as the option's argument.

Provide a path to the desired output file with `--file` or `-f` `<path/to/file>`.

The output will be appended to the file, if the file already exists.
#### Example
`muma --yt-playlist-to-local https://youtube.com/playlist?list=PLKUA473MWUv03VnZLb98iAxdbCLxl0qG3 --file ~/Music/myplaylist.toml`
### Environment variables
+ `YTDL_PATH` Provide the path to the youtube-dl binary to be used. Could be used to use yt-dlp instead for example.  Default binary path: `/usr/bin/youtube-dl`
+ `FFMPEG_PATH` Provide the path to the FFmpeg binary to be used.  Default binary path: `/usr/bin/ffmpeg`

## Bugs 
If you encounter any bugs, please report them here: https://codeberg.org/venji10/muma/issues

## Build instructions
1. Install the [dependencies](#dependencies)
2. Clone the project using git: `git clone https://codeberg.org/venji10/muma.git`
3. Go into the source directory: `cd muma`
4. Compile the program: `make`
5. You should have a binary named `muma` in the source directory
6. You can also compile and install the program using `sudo make install`

## Developing
+ If you want to contribute, that's great! You can help implement features from the [TODO](#todo).
If you want to implement something else, please open an issue first.
+ Please use the same naming / coding style I use.
    + Tab should be 4 spaces
    + Maximum line length should be 90. Exception for some print statements.
    + For other things, please look at the code.
+ There is a project config for [YCM](https://github.com/ycm-core/YouCompleteMe) if you use vim.
+ Use `make DEBUG=1` if you want to enable debugging compiler flags.

## Dependencies
+ [FFmpeg](https://ffmpeg.org/)
+ [libcurl](https://curl.se/libcurl/)
+ [TagLib](https://taglib.org/)
+ [tomlc99](https://github.com/cktan/tomlc99)
+ [youtube-dl](https://yt-dl.org/)

## License
[![gplv3](https://www.gnu.org/graphics/gplv3-with-text-136x68.png)](https://www.gnu.org/licenses/gpl-3.0.html)

Muma is free software licensed under the GNU GPLv3+.
See [LICENSE](./LICENSE) for the full license text.
